import Utils from "./utils.js";

class EventMenuModel {
  constructor(toggle) {
    this.detail = {
      toggle: toggle,
    };
  }
}

class Menu extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    //const root = this.attachShadow({ mode: "open" });
    const tpl = document.createElement("div");
    tpl.innerHTML = `
            <div class="menu-hamburger">
        <div class="menu-open">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" />
            </svg>
        </div>
        <div class="x-box">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </div>
        <slot></slot>
    </div>

    <div class="overlay"></div>
    `;
    document.body.appendChild(tpl);

    this.ui = Utils.uiHandler(
      [
        ".menu-hamburger",
        "MENU_TOGGLE",
        (evt) => this.setOverlay(this.open(evt.detail.toggle)),
      ],
      [
        ".menu-open",
        "click",
        (evt) => {
          this.ui.menuHamburger.classList.add("show-close");
          this.ui.menuHamburger.dispatchEvent(
            new CustomEvent("MENU_TOGGLE", new EventMenuModel(true))
          );
          evt.stopPropagation();
        },
      ],
      [
        ".x-box",
        "click",
        (evt) => {
          this.ui.menuHamburger.classList.remove("show-close");
          this.ui.menuHamburger.dispatchEvent(
            new CustomEvent("MENU_TOGGLE", new EventMenuModel(false))
          );
          evt.stopPropagation();
        },
      ],
      [
        ".menu-box",
        "click",
        (evt) => {
          const target = evt.target;
          evt.preventDefault();
          const targetIsAnchor = target instanceof HTMLAnchorElement && target;
          const parentIsAnchor =
            target.parentNode instanceof HTMLAnchorElement && target.parentNode;
          const anchor = targetIsAnchor || parentIsAnchor;
          return anchor && !anchor.className.includes("btn-active") && anchor;
        },
        (anchor) => {
          if (anchor) {
            this.setOverlay(this.open(false));
            location.href = anchor.href || "/";
          }
        },
      ],
      [
        ".overlay",
        "click",
        () => this.setOverlay(this.open(false)),
        () => this.ui.menuHamburger.classList.remove("show-close"),
      ]
    );
  }

  setOverlay(show) {
    this.ui.overlay.style.visibility = show ? "visible" : "hidden";
    this.ui.overlay.style.opacity = show ? 0.8 : 0;
  }

  open(visible) {
    this.ui.menuBox.classList[visible ? "add" : "remove"]("opened");
    return visible;
  }

  disconnectedCallback() {
    document.body.querySelector(".menu-hamburger")?.remove();
  }

  attributeChangedCallback(name, oldVal, newVal) {}

  adoptedCallback() {}
}

window.customElements.define("ui-menu", Menu);
