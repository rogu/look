const Utils = {

    dashToCamelCase(str) {
        return str.replace(/(-\w)/g, value => value[1].toUpperCase())
    },

    uiHandler(...args) {
        const ui = {};
        try {
            function createEl(selector, event, ...cbs) {
              let key;
              if (selector instanceof Node)
                ui[Utils.dashToCamelCase(selector.nodeName.substring(1))] =
                  selector;
              else {
                const isIdOrClass = /\.|#/.test(selector);
                key = Utils.dashToCamelCase(
                  isIdOrClass ? selector.substring(1) : selector
                );
                !ui[key] && (ui[key] = document.querySelector(selector));
              }
              event &&
                (key ? ui[key] : selector).addEventListener(event, (evt) => {
                  let param = evt;
                  cbs.forEach((cb) => (param = cb(param)));
                });
            }
            args.forEach((item) => createEl.call(null, ...item));
        } catch (error) {
            debugger;
        }
        return ui;
    }

};

export default Utils;
